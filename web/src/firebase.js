import { firebase } from '@firebase/app'
import '@firebase/auth'
import '@firebase/firestore'
let firebaseApp

const developmentApp = {
  apiKey: 'AIzaSyBhv9WG2_bMweS9nhCYTtOuz94wb9cR5xo',
  authDomain: 'harmony-development.firebaseapp.com',
  databaseURL: 'https://harmony-development.firebaseio.com',
  projectId: 'harmony-development',
  storageBucket: 'harmony-development.appspot.com',
  messagingSenderId: '145398373164',
  appId: '1:145398373164:web:f01568868947c8fc78ea89'
}

const productionApp = {
  apiKey: 'AIzaSyDgv7ksh4ckL8hm-j_AqzsLfaellPZsgvw',
  authDomain: 'harmony-production.firebaseapp.com',
  databaseURL: 'https://harmony-production.firebaseio.com',
  projectId: 'harmony-production',
  storageBucket: 'harmony-production.appspot.com',
  messagingSenderId: '54487118224',
  appId: '1:54487118224:web:c88114fedeb03752794315'
}

if (process.env.NODE_ENV === 'production') {
  firebaseApp = firebase.initializeApp(productionApp)
} else {
  firebaseApp = firebase.initializeApp(developmentApp)
}

export { firebase }

export const db = firebaseApp.firestore()
