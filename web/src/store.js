import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'
import { firebase } from './firebase'
import { queryBuilder, createOne, updateOne } from './db'

Vue.use(Vuex)

export const LOGIN_ACTION = 'LOGIN_ACTION'
export const LOGOUT_ACTION = 'LOGOUT_ACTION'
export const GET_COLLECTION_ACTION = 'GET_COLLECTION_ACTION'
export const GET_INITIAL_APP_DATA = 'GET_INITIAL_APP_DATA'
export const CREATE_ITEM_ACTION = 'CREATE_ITEM_ACTION'
export const UPDATE_ITEM_ACTION = 'UPDATE_ITEM_ACTION'
export const DELETE_ITEM_ACTION = 'DELETE_ITEM_ACTION'
export const SET_SNACKBAR_ACTION = 'SET_SNACKBAR_ACTION'
export const SET_SELECTED_COLLECTION_ACTION = 'SET_SELECTED_COLLECTION_ACTION'
export const TOGGLE_DRAWER_ACTION = 'TOGGLE_DRAWER_ACTION'

export const LOGIN_MUTATION = 'LOGIN_MUTATION'
export const LOGOUT_MUTATION = 'LOGOUT_MUTATION'
const SET_COLLECTION_MUTATION = 'SET_COLLECTION_MUTATION'
const SET_SNACKBAR_MUTATION = 'SET_SNACKBAR_MUTATION'
const SET_SELECTED_COLLECTION_MUTATION = 'SET_SELECTED_COLLECTION_MUTATION'
const TOGGLE_DRAWER_MUTATION = 'TOGGLE_DRAWER_MUTATION'
const INCREASE_APP_LOADING_MUTATION = 'INCREASE_APP_LOADING_MUTATION'
const DECREASE_APP_LOADING_MUTATION = 'DECREASE_APP_LOADING_MUTATION'

export const TABLE_HEADERS_GETTER = 'TABLE_HEADERS_GETTER'
export const SELECTED_COLLECTION_ITEMS_GETTER = 'SELECTED_COLLECTION_ITEMS_GETTER'

const collections = {
  users: [],
  fields: [],
  sermonSeries: [],
  sermons: [],
  regroups: [],
  worshipAlbums: [],
  worshipSongs: [],
  events: [],
  jobs: [],
  jobTypes: [],
  notifications: [],
  notificationTypes: []
}

const getDefaultState = () => {
  return {
    appLoading: 0,
    snackbar: {
      show: false,
      text: '',
      color: ''
    },
    loggedIn: false,
    drawerOpen: false,
    selectedCollectionName: 'users',
    user: {},
    ...collections
  }
}

const state = getDefaultState()

const getters = {
  [TABLE_HEADERS_GETTER]: state => {
    const tableFields = state.fields.filter(field => field.collection_name.includes(state.selectedCollectionName))
    return tableFields.sort((a, b) => {
      return a.column_order - b.column_order
    })
  },
  [SELECTED_COLLECTION_ITEMS_GETTER]: state => {
    return state[`${state.selectedCollectionName}`]
  }
}

const actions = {
  [TOGGLE_DRAWER_ACTION] ({ commit }) {
    commit(TOGGLE_DRAWER_MUTATION)
  },
  [SET_SNACKBAR_ACTION] ({ commit }, { show, text, color }) {
    commit(SET_SNACKBAR_MUTATION, { show, text, color })
  },
  async [DELETE_ITEM_ACTION] ({ dispatch, commit, state }, oldItem) {
    commit(INCREASE_APP_LOADING_MUTATION)
    try {
      await updateOne({
        id: oldItem.uid,
        collection: state.selectedCollectionName,
        oldItem,
        updateSet: { deleted: true }
      })
      dispatch(SET_SNACKBAR_ACTION, {
        show: true,
        text: `${state.selectedCollectionName} item was deleted.`,
        color: 'success'
      })
      await dispatch(GET_COLLECTION_ACTION, state.selectedCollectionName)
      commit(DECREASE_APP_LOADING_MUTATION)
    } catch (error) {
      dispatch(SET_SNACKBAR_ACTION, {
        show: true,
        text: `There was a problem deleting your ${state.selectedCollectionName} item.`,
        color: 'error'
      })
      commit(DECREASE_APP_LOADING_MUTATION)
    }
  },
  async [UPDATE_ITEM_ACTION] ({ dispatch, commit, state }, { id, updateSet, oldItem }) {
    commit(INCREASE_APP_LOADING_MUTATION)
    try {
      await updateOne({
        id,
        collection: state.selectedCollectionName,
        oldItem,
        updateSet
      })
      dispatch(SET_SNACKBAR_ACTION, {
        show: true,
        text: `${state.selectedCollectionName} item updated.`,
        color: 'success'
      })
      await dispatch(GET_COLLECTION_ACTION, state.selectedCollectionName)
      commit(DECREASE_APP_LOADING_MUTATION)
    } catch (error) {
      dispatch(SET_SNACKBAR_ACTION, {
        show: true,
        text: `There was a problem updating your ${state.selectedCollectionName} item.`,
        color: 'error'
      })
      commit(DECREASE_APP_LOADING_MUTATION)
    }
  },
  async [CREATE_ITEM_ACTION] ({ dispatch, commit, state }, newItem) {
    commit(INCREASE_APP_LOADING_MUTATION)
    try {
      await createOne({
        collection: state.selectedCollectionName,
        updateSet: newItem
      })
      dispatch(SET_SNACKBAR_ACTION, {
        show: true,
        text: `New ${state.selectedCollectionName} item created.`,
        color: 'success'
      })
      await dispatch(GET_COLLECTION_ACTION, state.selectedCollectionName)
      commit(DECREASE_APP_LOADING_MUTATION)
    } catch (error) {
      dispatch(SET_SNACKBAR_ACTION, {
        show: true,
        text: `There was a problem creating your ${state.selectedCollectionName} item.`,
        color: 'error'
      })
      commit(DECREASE_APP_LOADING_MUTATION)
    }
  },
  async [SET_SELECTED_COLLECTION_ACTION] ({ state, commit }, collectionName) {
    if (state.selectedCollectionName !== collectionName) {
      commit(SET_SELECTED_COLLECTION_MUTATION, collectionName)
    }
  },
  async [GET_INITIAL_APP_DATA] ({ dispatch }) {
    Object.keys(collections).forEach(collectionName => {
      dispatch(GET_COLLECTION_ACTION, collectionName)
    })
  },
  async [GET_COLLECTION_ACTION] ({ commit }, collectionName) {
    const activeItemsQuery = {
      field: 'deleted',
      comparison: '==',
      value: false
    }
    commit(INCREASE_APP_LOADING_MUTATION)
    const collectionItems = await queryBuilder({
      collection: collectionName,
      queries: [activeItemsQuery]
    })
    commit(SET_COLLECTION_MUTATION, { collectionName, collectionItems })
    commit(DECREASE_APP_LOADING_MUTATION)
  },
  async [LOGIN_ACTION] ({ state, commit, dispatch }, { email, password }) {
    if (state.loggedIn) {
      return
    }
    try {
      commit(INCREASE_APP_LOADING_MUTATION)
      await firebase.auth().signInWithEmailAndPassword(email, password)
    } catch (error) {
      dispatch(SET_SNACKBAR_ACTION, {
        show: true,
        text: 'There was a problem trying to log in.',
        color: 'error'
      })
    }
    commit(DECREASE_APP_LOADING_MUTATION)
  },
  async [LOGOUT_ACTION] ({ commit, dispatch }) {
    if (!state.loggedIn) {
      return
    }
    try {
      commit(INCREASE_APP_LOADING_MUTATION)
      await firebase.auth().signOut()
    } catch (error) {
      dispatch(SET_SNACKBAR_ACTION, {
        show: true,
        text: 'There was a problem trying to log out.',
        color: 'error'
      })
    }
    commit(DECREASE_APP_LOADING_MUTATION)
  }
}

const mutations = {
  [TOGGLE_DRAWER_MUTATION] (state) {
    Vue.set(state, 'drawerOpen', !state.drawerOpen)
  },
  [SET_SNACKBAR_MUTATION] (state, { show, text, color }) {
    Vue.set(state, 'snackbar', { show, text, color })
  },
  [INCREASE_APP_LOADING_MUTATION] (state) {
    Vue.set(state, 'appLoading', state.appLoading + 1)
  },
  [DECREASE_APP_LOADING_MUTATION] (state) {
    Vue.set(state, 'appLoading', state.appLoading ? state.appLoading - 1 : 0)
  },
  [LOGIN_MUTATION] (state, user) {
    Vue.set(state, 'loggedIn', true)
    Vue.set(state, 'user', { ...user })
    router.push({ path: '/' })
  },
  [LOGOUT_MUTATION] (state) {
    Object.assign(state, getDefaultState())
    router.push({ path: '/login' })
  },
  [SET_COLLECTION_MUTATION] (state, { collectionName, collectionItems }) {
    Vue.set(state, `${collectionName}`, [...collectionItems])
  },
  [SET_SELECTED_COLLECTION_MUTATION] (state, collectionName) {
    Vue.set(state, 'selectedCollectionName', collectionName)
  }
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
