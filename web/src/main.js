import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store, { LOGIN_MUTATION, LOGOUT_MUTATION, SET_SNACKBAR_ACTION } from './store'
import vuetify from './plugins/vuetify'
import { firebase } from './firebase'

Vue.config.productionTip = false

let app = ''

firebase.auth().onAuthStateChanged(async (user) => {
  if (user) {
    try {
      store.commit(LOGIN_MUTATION, user)
    } catch (error) {
      store.dispatch(SET_SNACKBAR_ACTION, {
        show: true,
        text: 'There was an authentication error sent from the database service.',
        color: 'error'
      })
    }
  } else {
    store.commit(LOGOUT_MUTATION)
  }

  if (!app) {
    app = new Vue({
      router,
      store,
      vuetify,
      render: h => h(App)
    }).$mount('#app')
  }
})
