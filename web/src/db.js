import { db, firebase } from './firebase'
import store from './store'
import * as moment from 'moment'

export const dateFields = [
  'created_at',
  'modified_at',
  'start_date',
  'end_date',
  'sermon_date'
]

export const userFields = [
  'leader_uid',
  'speaker_uid',
  'member_uids',
  'musician_uids',
  'assignee_uid',
  'sender_uid',
  'reciever_uids'
]

export const nonUserUidFields = [
  {
    field: 'worship_album_uid',
    displayField: 'title',
    collectionName: 'worshipAlbums'
  },
  {
    field: 'sermon_series_uid',
    displayField: 'title',
    collectionName: 'sermonSeries'
  }
]

export const getOne = async ({ id, collection }) => {
  const doc = await db.collection(collection).doc(id).get()
  if (!doc.exists) {
    return null
  }
  return doc.data()
}

export const removeOne = async ({ id, collection }) => {
  return db.collection(collection).doc(id).delete()
}

export const getAll = async ({ collection }) => {
  const snapshot = await db.collection(collection).get()
  return iterateSnapshotForItems(snapshot)
}

export const queryBuilder = async ({ collection, queries }) => {
  const ref = db.collection(collection)
  let firestoreQuery
  queries.forEach((query, index) => {
    if (index === 0) {
      firestoreQuery = ref.where(query.field, query.comparison, query.value)
    } else {
      firestoreQuery.where(query.field, query.comparison, query.value)
    }
  })
  const snapshot = await firestoreQuery.get()
  return iterateSnapshotForItems(snapshot)
}

export const updateOne = async ({ id, collection, oldItem, updateSet }) => {
  return db.collection(collection).doc(id).set({
    ...applyBaseSchema(),
    ...oldItem,
    ...updateSet
  })
}

export const createOne = async ({ collection, updateSet, id }) => {
  let uid
  if (id) {
    uid = id
  } else {
    const newRef = await generateNewDocRef(collection)
    uid = newRef.id
  }
  await db.collection(collection).doc(uid).set({
    uid,
    ...applyBaseSchema(true),
    ...updateSet
  })
  return getOne({ id: uid, collection })
}

export const updateMultiple = async (refSetArray) => {
  return db.runTransaction(async t => {
    for (const refSet of refSetArray) {
      await t.update(db.collection(refSet.collection).doc(refSet.id), {
        ...applyBaseSchema(),
        ...refSet.updateSet
      })
    }
  })
}

export const createMultiple = async (refSetArray) => {
  return db.runTransaction(async t => {
    for (const refSet of refSetArray) {
      const newRef = refSet.ref ? refSet.ref : await generateNewDocRef(refSet.collection)
      const uid = newRef.id
      await t.set(newRef, {
        uid,
        ...applyBaseSchema(true),
        ...refSet.updateSet
      })
    }
  })
}

export const updateOrCreateMultiple = async ({ updateRefSetArray, createRefSetArray }) => {
  return db.runTransaction(async t => {
    for (const refSet of createRefSetArray) {
      const newRef = refSet.ref ? refSet.ref : await generateNewDocRef(refSet.collection)
      const uid = newRef.id
      await t.set(newRef, {
        uid,
        ...applyBaseSchema(true),
        ...refSet.updateSet
      })
    }
    for (const refSet of updateRefSetArray) {
      await t.update(db.collection(refSet.collection).doc(refSet.id), {
        ...applyBaseSchema(),
        ...refSet.updateSet
      })
    }
  })
}

async function generateNewDocRef (collection) {
  return db.collection(collection).doc()
}

function iterateSnapshotForItems (snapshot) {
  if (snapshot.empty) {
    return []
  }
  const unsanitizedItems = snapshot.docs.map(item => item.data())
  return sanitizeFields(unsanitizedItems)
}

function sanitizeFields (items) {
  return items.map(i => {
    dateFields.forEach(fieldName => {
      if (i[fieldName]) {
        i[fieldName] = moment(i[fieldName].toDate()).format('MM/DD/YYYY LT')
      }
    })
    return i
  })
}

function applyBaseSchema (isNew) {
  const base = {
    modified_at: firebase.firestore.FieldValue.serverTimestamp(),
    modified_by: store.state.user.uid,
    deleted: false,
    version: 1
  }
  const newItem = {
    created_at: firebase.firestore.FieldValue.serverTimestamp(),
    created_by: store.state.user.uid
  }

  return isNew ? Object.assign(newItem, base) : base
}
