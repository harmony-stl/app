import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    options: {
      customProperties: true
    },
    themes: {
      light: {
        primary: '#7E5773',
        secondary: '#B0AEAF',
        accent: '#54214F',
        error: '#FF5A44',
        info: '#479AFF',
        success: '#37C660',
        warning: '#FFCC00',
        // override default anchor colors
        anchor: '#479AFF',
        // harmony theme styles
        // inside Vue component style blocks
        // tinyLabel would be var(--v-tinyLabel-base)
        white: '#ffffff',
        black: '#000000',
        gray: '#222b45',
        grayDark: '#b0aeaf',
        grayLight: '#edf1f7',
        grayPale: '#e4e9f2'

      }
    }
  }
})
