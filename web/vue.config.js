module.exports = {
  lintOnSave: false,
  transpileDependencies: [
    'vuetify'
  ],
  css: {
    loaderOptions: {
      scss: {
        prependData: '@import "@/sass/variables.scss";'
      }
    }
  }
}
