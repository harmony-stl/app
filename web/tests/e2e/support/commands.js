// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
Cypress.Commands.add('getLabel', { prevSubject: 'optional' }, (subject, label) => {
  if (subject) {
    return cy.wrap(subject).find(`*[aria-label="${label}"]`)
  } else {
    return cy.get(`*[aria-label="${label}"]`)
  }
})

Cypress.Commands.add('login', () => {
  cy.contains('Email address').siblings('input').click().type('test@example.com', { force: true })
  cy.contains('Password').siblings('input').click().type('Pass123!')
  cy.getLabel('Login Button').click()
})

Cypress.Commands.add('seedDb', () => {
  cy.exec('node tests/e2e/support/seedDb.js')
})

Cypress.Commands.add('clearUsers', () => {
  cy.exec('node tests/e2e/support/clearUsers.js')
})

Cypress.Commands.add('clearFields', () => {
  cy.exec('node tests/e2e/support/clearFields.js')
})

Cypress.Commands.add('clearSermonSeries', () => {
  cy.exec('node tests/e2e/support/clearSermonSeries.js')
})
