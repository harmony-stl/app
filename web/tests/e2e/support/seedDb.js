var db = require('./firebase.js')

var users = [{
  email: 'test@example.com',
  first_name: 'John',
  last_name: 'Doe',
  phone_number: '3147993577',
  display_contact_info: true,
  serving_reminder: ['One day'],
  is_admin: true,
  is_volunteer: true
}]

var sermonSeries = [{
  title: 'Belonging',
  description: 'What does it mean to belong to God? It means that you do not belong to your pain, your past, your problems, the brokenness of this world. It means freedom. Life. Purpose. And so much more.',
  image_url: 'https://firebasestorage.googleapis.com/v0/b/harmony-development.appspot.com/o/images%2Fsermon3.png?alt=media&token=3d330900-cb06-43c5-a477-3ec75130844a',
  start_date: db.firestoreDate(new Date('January 1, 2020')),
  end_date: db.firestoreDate(new Date('January 31, 2020'))
}]

var fields = [{
  text: 'First Name',
  value: 'first_name',
  sortable: true,
  column_order: 1,
  type: 'text',
  collection_name: ['users'],
  display_importance: 'important',
  deleted: false
}, {
  text: 'Last Name',
  value: 'last_name',
  sortable: true,
  column_order: 2,
  type: 'text',
  collection_name: ['users'],
  display_importance: 'important',
  deleted: false
}, {
  text: 'Email',
  value: 'email',
  sortable: true,
  column_order: 3,
  type: 'text',
  collection_name: ['users'],
  display_importance: 'critical',
  deleted: false
}, {
  text: 'Created At',
  value: 'created_at',
  sortable: true,
  column_order: 4,
  type: 'date',
  collection_name: ['users', 'sermonSeries'],
  deleted: false
}, {
  text: 'Title',
  value: 'title',
  sortable: true,
  column_order: 1,
  type: 'text',
  collection_name: ['sermonSeries'],
  deleted: false,
  display_importance: 'critical'
}, {
  text: 'Description',
  value: 'description',
  sortable: true,
  column_order: 2,
  type: 'textarea',
  collection_name: ['sermonSeries'],
  deleted: false,
  display_importance: 'important'
}, {
  text: 'Image',
  value: 'image_url',
  sortable: true,
  column_order: 3,
  type: 'image',
  collection_name: ['sermonSeries'],
  deleted: false
}, {
  text: 'Start Date',
  value: 'start_date',
  sortable: true,
  column_order: 4,
  type: 'date',
  collection_name: ['sermonSeries'],
  deleted: false
}, {
  text: 'End Date',
  value: 'end_date',
  sortable: true,
  column_order: 5,
  type: 'date',
  collection_name: ['sermonSeries'],
  deleted: false
}]

var createRefSet = []
users.forEach(user => createRefSet.push({
  collection: 'users',
  updateSet: user
}))
fields.forEach(header => createRefSet.push({
  collection: 'fields',
  updateSet: header
}))
sermonSeries.forEach(series => createRefSet.push({
  collection: 'sermonSeries',
  updateSet: series
}))

db.createMultiple(createRefSet).then(r => {
  process.exit(0)
}).catch(error => {
  throw error
})
