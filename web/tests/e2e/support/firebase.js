var admin = require('firebase-admin')

var serviceAccount = require('../../../harmony-development-firebase-adminsdk.json')

if (process.env.CI_PROJECT_NAME) {
  // we are in gitlab CI
  admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: 'https://harmony-development.firebaseio.com'
  })
} else {
  // we are running locally
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://harmony-development.firebaseio.com'
  })
}

var db = admin.firestore()

var firestoreDate = (javascriptDate) => {
  return admin.firestore.Timestamp.fromDate(javascriptDate)
}

var createOne = async ({ collection, updateSet, id }) => {
  var uid
  if (id) {
    uid = id
  } else {
    var newRef = await generateNewDocRef(collection)
    uid = newRef.id
  }
  return db.collection(collection).doc(uid).set({
    uid,
    ...updateSet
  })
}

var createMultiple = async (refSetArray) => {
  return db.runTransaction(async t => {
    for (var refSet of refSetArray) {
      var newRef = refSet.ref ? refSet.ref : await generateNewDocRef(refSet.collection)
      var uid = newRef.id
      await t.set(newRef, {
        uid,
        ...applyBaseSchema(true),
        ...refSet.updateSet
      })
    }
  })
}

var queryBuilder = async ({ collection, queries }) => {
  var ref = db.collection(collection)
  var firestoreQuery
  queries.forEach((query, index) => {
    if (index === 0) {
      firestoreQuery = ref.where(query.field, query.comparison, query.value)
    } else {
      firestoreQuery.where(query.field, query.comparison, query.value)
    }
  })
  var snapshot = await firestoreQuery.get()
  if (snapshot.empty) {
    return []
  }
  return snapshot.docs.map(item => item.data())
}

var removeOne = async function ({ id, collection }) {
  return db.collection(collection).doc(id).delete()
}

var removeTesterDataFromCollection = async function (collection) {
  var results = await queryBuilder({
    collection,
    queries: [{
      field: 'created_by',
      comparison: '==',
      value: 'automated_tester'
    }]
  })
  var promises = []
  results.forEach(async function (item) {
    promises.push(removeOne({
      id: item.uid,
      collection
    }))
  })
  await Promise.all(promises)
}

function applyBaseSchema (isNew) {
  var base = {
    modified_at: admin.firestore.FieldValue.serverTimestamp(),
    modified_by: 'automated_tester',
    deleted: false,
    version: 1
  }
  var newItem = {
    created_at: admin.firestore.FieldValue.serverTimestamp(),
    created_by: 'automated_tester'
  }

  return isNew ? Object.assign(newItem, base) : base
}

async function generateNewDocRef (collection) {
  return db.collection(collection).doc()
}

exports.createOne = createOne
exports.createMultiple = createMultiple
exports.queryBuilder = queryBuilder
exports.removeTesterDataFromCollection = removeTesterDataFromCollection
exports.firestoreDate = firestoreDate
