describe('Login', () => {
  it('should only log in with valid email', () => {
    cy.contains('Harmony Administrator Login').should('exist')
    cy.contains('Email address').siblings('input').click().type('fsadkjfs', { force: true })
    cy.contains('Enter a valid email address.').should('exist')

    cy.contains('Email address').siblings('input').click().clear().type('test@example.com')
    cy.contains('Password').siblings('input').click().type('Pass12')
    cy.contains('Password must be at least 4 characters and contain an uppercase letter, lowercase letter, number, and special character.').should('exist')
    cy.contains('Password').siblings('input').click().clear().type('Pass123!')
    cy.getLabel('Login Button').click()

    cy.contains('Users').should('exist')
  })

  it('should log out and return to login view', () => {
    cy.login()
    cy.contains('Logout').click()
    cy.contains('Harmony Administrator Login').should('exist')
  })
})
