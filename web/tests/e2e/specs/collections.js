describe('Collections', () => {
  it('should default to users collection', () => {
    cy.login()

    cy.contains('Users').should('exist')
    cy.contains('John').should('exist')
    cy.contains('Doe').should('exist')
  })

  it('should be able to switch to sermon series collection', () => {
    cy.login()

    cy.getLabel('Harmony Navigation Menu').click()
    cy.contains('SermonSeries').click()
    cy.getLabel('Harmony Navigation Menu').click()

    cy.contains('SermonSeries').should('exist')
    cy.contains('Belonging').should('exist')
  })
})
