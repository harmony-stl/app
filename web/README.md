# Admin Dashboard
This is the web app interface for Harmony leadership to update the content for the mobile apps. Assume all commands below are run relative to this `/web` directory as opposed to the monorepo root directory.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

## Deploying to Firebase hosting
The app is deployed as a static single-page web app to firebase hosting. Here is basic information about [how deployment with Firebase works](https://cli.vuejs.org/guide/deployment.html#firebase). And at a higher level, here is [how hosting with Firebase works](https://firebase.google.com/docs/hosting).

#### Development hosting
`npm run deploy:dev` will build the app with NODE_ENV environment variable set to "development". It then deploys the contents of the `/dist` folder to https://harmony-development.firebaseapp.com

#### Production hosting
`npm run deploy:prod` will build the app with NODE_ENV as "production". It then deploys the contents of the `/dist` folder to https://harmony-production.firebaseapp.com
