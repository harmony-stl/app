# Harmony App

Monorepo for the Harmony App and its related components

| Module              | Description             |
| ------------------- | ----------------------- |
| [mobile](./mobile)  | Flutter mobile app      |
| [web](./web)        | Admin dashboard web app |
| [design](./design)  | Design Assets           |

## Resources

A list of common resources and infrastructure for managing the project

| Name                | Description             |
| ------------------- | ----------------------- |
| [Code Repo](https://gitlab.com/harmony-stl/app)                    | Gitlab repo for all source code                        |
| [Discord Server](https://discord.gg/PeXpBFS)       | Discord chat for coordinating work             |
| [Trello Board](https://trello.com/b/F3nkkwmq/harmony-app)  | Trello board for managing work           |
| [Dev Database](https://console.firebase.google.com/project/harmony-development/overview)  | Development database project in Google Cloud      |
| [Prod Database](https://console.firebase.google.com/project/harmony-production/overview)   | Production database project in Google Cloud |
| [Dev Admin Webapp](harmony-development.firebaseapp.com)   | Development Admin App hosted in Firebase |
| [Prod Admin Webapp](harmony-production.firebaseapp.com)   | Production Admin App hosted in Firebase |

## Platform specific build configuration
While Flutter aims to be a cross-platform codebase, there are some things that need to get setup for deploying to each kind of app environment.

#### Package dependencies
You'll notice right away some contradictory information in the form of Google Firestore documentation walking you through how to setup an iOS app integration to firestore using coacoa pods and pod installations. This is **NOT** the way you use Flutter. For Flutter, it has its own package system managed through the `pubspec.yaml` file in the root of the `/mobile` directory.
> While seems contradictory, google does have a specific guide on Flutter setup which has the correct instructions for us: https://firebase.google.com/docs/flutter/setup?platform=android (toggle between iOS and Android)

Flutter needs to manage dependencies in this way because it internally will ensure what you are requiring get the platform-specific versions that work for both iOS and Android.

#### iOS build configuration
* App bundle identifier is com.harmonystl-dev.mobile
> **Note** I added the google.plist from firestore to the Runner folder via Xcode because it will handle updating the Targets (Not sure yet what this means). If you simply drop the file there through another IDE or File Explorer it does not do that.

#### Android build configuration
* Android package name is com.harmonystldev.mobile
 