Harmony data model
==================

# Base (applies to all schemas)
| Property     | Type           |
| ------------ | -------------- |
| uid          | unique         |
| created_at   | timestamp      |
| modified_at  | timestamp      |
| created_by   | user uid       |
| modified_by  | user uid       |
| deleted      | boolean        |
| version      | number         |

# Fields
| Property             | Type                                                        |
| -------------------- | ----------------------------------------------------------- |
| text                 | string                                                      |
| value                | string                                                      |
| type                 | string[text, textarea, date, select, checkbox, reference]   |
| collection_name      | array of collection names                                   |
| display_importance   | nullable string[critical, important]                        |
| editable             | nullable boolean                                            |
| sortable             | nullable boolean                                            |
| column_order         | number                                                      |
| class                | nullable string                                             |
| values_of_select     | array of strings (only used if type is select)              |
| ref_display_name     | string (only used if type is reference)                     |
| ref_collection_name  | string (only used if type is reference)                     |

# User
| Property              | Type                          |
| --------------------- | ----------------------------- |
| email                 | string                        |
| first_name            | string                        |
| last_name             | string                        |
| phone_number          | nullable string               |
| display_contact_info  | boolean                       |
| serving_reminder      | array of [1d, 3d, 1wk, none]  |
| is_admin              | boolean                       |
| is_volunteer          | boolean                       |

# SermonSeries
| Property     | Type            |
| ------------ | --------------- |
| title        | string          |
| description  | nullable string |
| image_url    | nullable string |
| start_date   | timestamp       |
| end_date     | timestamp       |

# Sermon
| Property          | Type               |
| ----------------- | ------------------ |
| sermon_series_uid | sermon series uid  |
| speaker_uid       | user uid           |
| sermon_date       | timestamp          |
| audio_url         | string             |
| duration          | number             |
| notes             | nullable string    |

# ReGroup
| Property     | Type                |
| ------------ | ------------------- |
| leader_uid   |  user uid           |
| member_uids  |  array of user uids |
| title        |  string             |
| description  |  nullable string    |
| image_url    |  nullable string    |

# WorshipAlbum
| Property     | Type             |
| ------------ | ---------------- |
| title        | string           |
| description  | nullable string  |
| image_url    | nullable string  |

# WorshipSong
| Property           | Type               |
| ------------------ | ------------------ |
| worship_album_uid  | worship album uid  |
| musician_uids      | array of user uids |
| title              | string             |
| audio_url          | string             |
| duration           | number             |

# Event
| Property     | Type               |
| ------------ | ------------------ |
| title        | string             |
| image_url    | nullable string    |
| description  | nullable string    |
| start_time   | timestamp          |
| end_time     | nullable timestamp |

# Job
| Property     | Type              |
| ------------ | ----------------- |
| job_type_uid | jobType uid       |
| event_uid    | event uid         |
| assignee_uid | nullable user uid |

# JobType
| Property     | Type            |
| ------------ | --------------- |
| title        | string          |
| description  | nullable string |

# Notification
| Property           | Type                           |
| ------------------ | ------------------------------ |
| sender_uid         | user uid                       |
| receiver_uids      | array of user uids             |
| type_uids          | array of NotificationType uids |
| related_job_uid    | nullable job uid               |
| message            | nullable string                |
| receiver_read      | boolean                        |

# NotificationType
| Property     | Type              |
| ------------ | ----------------- |
| title        | string            |
