# Design Assets
**All UI design resources are kept out of the repo and in Google Drive**. Project contributors have edit access, but everyone else with the link has view access: https://drive.google.com/drive/folders/1jxr2_6-MXDOcOqGde0POexfZ1QccnXA0?usp=sharing.

## Database Design
* [Planned Schema]('./dataModel.md') - and for schema migrations we will write update scripts, as needed.

#### Eva Light
The UI design material is found inside the [Eva Light Design System](https://eva.design/) sketch file using Sketch - Version 58 (84663). Using the design system makes rapid prototyping easier and more consistent. It also sets a stage for future iteration on the design of the apps. The design system allows you to change the color palettes, typography, and default component styles with single changes that cascade through everywhere they are used in the design file.

#### Fonts
When opening for the first time, you may need to manually install the Quicksand, Averia_sans_libr, and Open_Sans fonts to your OS. You can find these in `/assets/fonts/` in Google Drive.

## Licensing
Eva Light design system is self-described as "Free and Open Source.". We have included the license that was attached at the time of use as it specifies that copies of the "Software" cannot be re-distributed or sold. The "Software" here is simply the source sketch file and the resources that that file uses. Since we won't include the design files in the mobile apps or web app we will be neither distributing or selling the Software under license.

[Quicksand](https://fonts.google.com/specimen/Quicksand), [Averia_Sans_Libre](https://fonts.google.com/specimen/Averia+Sans+Libre), and [Open_Sans](https://fonts.google.com/specimen/Open+Sans) fonts are all Open Source licensed and can be used in our apps, and those apps can be distributed and sold. The only restriction is that we cannot sell the fonts (or it's component ligatures, etc...) directly. There's hardly a reason Harmony would ever want to do that anyway.
