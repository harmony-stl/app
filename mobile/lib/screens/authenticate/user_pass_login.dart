import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobile/screens/authenticate/password_reset.dart';
import 'package:mobile/screens/authenticate/sign_up.dart';
import 'package:mobile/services/auth.dart';
import 'package:mobile/services/theme.dart';
import 'package:mobile/services/util.dart';

class UserPassLogin extends StatefulWidget {
  @override
  _UserPassLoginState createState() => _UserPassLoginState();
}

class _UserPassLoginState extends State<UserPassLogin> {
  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();

  String _email = '';
  String _password = '';
  String _error = '';

  Widget _displayIfFirebaseAuthError() {
    if (_error != '') {
      return Container(
        padding: EdgeInsets.only(top: 10),
        child: Text(
          _error,
          style: TextStyle(color: Colors.red, fontSize: 14.0),
          textDirection: TextDirection.ltr,
        ),
      );
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.chevron_left),
          onPressed: () => Navigator.pop(context, false),
        )
      ),
      body: new SingleChildScrollView(
        child: new Container(
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.all(20),
          decoration: harmonyDecoration,
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              new Container(
                width: 100,
                height: 100,
                child: SvgPicture.asset("assets/logoHarmony.svg", color: themeColors['white']),
              ),
              new Container(
                width: 375,
                height: 60,
                child: new Center(
                  child: new Text(
                    "Log In",
                    style: Theme.of(context).textTheme.display1,
                    textDirection: TextDirection.ltr,
                  ),
                ),
              ),
              new Form(
                key: _formKey,
                child: new Column(
                  children: <Widget>[
                    SizedBox(height: 50.0),
                    TextFormField(
                      style: TextStyle(
                        color: themeColors['gray'],
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                      decoration: InputDecoration(
                        hintText: 'Email address'
                      ).applyDefaults(
                        Theme.of(context).inputDecorationTheme,
                      ),
                      onChanged: (val) => setState(() { _email = val; }),
                      validator: (val) => validateEmail(val),
                    ),
                    SizedBox(height: 20.0),
                    TextFormField(
                      style: TextStyle(
                        color: themeColors['gray'],
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                      decoration: InputDecoration(
                        hintText: 'Password',
                        errorMaxLines: 3,
                      ).applyDefaults(
                        Theme.of(context).inputDecorationTheme,
                      ),
                      onChanged: (val) => setState(() { _password = val; }),
                      obscureText: true,
                      validator: (val) => validateComplexPassword(val),
                    ),
                  ],
                )
              ),
              _displayIfFirebaseAuthError(),
              new GestureDetector(
                onTap: () async {
                  if (_formKey.currentState.validate()) {
                    try {
                      await _auth.login(_email, _password);
                    } catch(e) {
                      setState(() => _error = e.message);
                    }
                  }
                },
                child: new Container(
                  width: 375,
                  height: 56,
                  margin: EdgeInsets.only(top: 20),
                  decoration: new BoxDecoration(
                    color: themeColors['grayDark'],
                    borderRadius: BorderRadius.circular(4),
                    boxShadow: [BoxShadow(
                        color: themeColors['darkBoxShadow'],
                        offset: Offset(0,2),
                        blurRadius: 4,
                        spreadRadius: 0
                    )],
                  ),
                  child: new Center(
                      child: new Text("LOG IN",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: themeColors['white'],
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                          decoration: TextDecoration.none,
                        ),
                        textDirection: TextDirection.ltr,
                      ),
                  ),
                ),
              ),
              new Container(
                margin: EdgeInsets.only(bottom: 100, top: 20),
                child: new Align(
                  alignment: Alignment(1, 0),
                  child: GestureDetector(
                    onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PasswordReset()),
                    ),
                    child: new Text(
                      "Forgot Password",
                      style: Theme.of(context).textTheme.subhead,
                      textDirection: TextDirection.ltr,
                    ),
                  ),
                ),
              ),
              new Center(
                child: GestureDetector(
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SignUp()),
                  ),
                  child: new Text(
                    "New to Harmony? Sign Up",
                    style: Theme.of(context).textTheme.subhead,
                    textDirection: TextDirection.ltr,
                  ),
                ),
              ),
            ],
          )
      ),
      ),
    );
  }
}
