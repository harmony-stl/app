import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobile/screens/authenticate/user_pass_login.dart';
import 'package:mobile/services/theme.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.chevron_left),
          onPressed: () => Navigator.pop(context, false),
        )
      ),
      body: new SingleChildScrollView(
        child: new Container(
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.all(20),
          decoration: harmonyDecoration,
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              new Container(
                width: 100,
                height: 100,
                child: SvgPicture.asset("assets/logoHarmony.svg", color: themeColors['white']),
              ),
              new Container(
                width: 375,
                height: 60,
                child: new Center(
                  child: new Text(
                    "Sign up",
                    style: Theme.of(context).textTheme.display1,
                    textDirection: TextDirection.ltr,
                  ),
                ),
              ),
              new Center(
                child: GestureDetector(
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => UserPassLogin()),
                  ),
                  child: new Text(
                    "Already have an account? Log in.",
                    style: Theme.of(context).textTheme.subhead,
                    textDirection: TextDirection.ltr,
                  ),
                ),
              ),
            ],
          )
      ),
      ),
    );
  }
}