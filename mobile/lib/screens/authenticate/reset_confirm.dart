import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobile/services/theme.dart';

class ResetConfirm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.chevron_left),
          onPressed: () => Navigator.pop(context, false),
        ),
      ),
      body: new Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        decoration: harmonyDecoration,
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(
              width: 164,
              height: 164,
              margin: EdgeInsets.only(bottom: 35),
              child: SvgPicture.asset(
                "assets/allDone.svg",
                color: themeColors['white'],
              ),
            ),
            new Container(
              margin: EdgeInsets.only(bottom: 25),
              child: new Text(
                "Email Sent!",
                style: Theme.of(context).textTheme.display1,
              ),
            ),
            new Container(
              margin: EdgeInsets.only(bottom: 100),
              child: new Text(
                "Check your inbox for a message from us with a link you can click to reset your password.\n\nDon’t forget to check your spam folder, if you don’t see it in your inbox!",
                style: Theme.of(context).textTheme.subhead,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
