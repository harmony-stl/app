import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobile/screens/authenticate/sign_up.dart';
import 'package:mobile/screens/authenticate/user_pass_login.dart';
import 'package:mobile/services/auth.dart';
import 'package:mobile/services/theme.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final AuthService _auth = AuthService();

  String _error = '';

  Widget _displayIfFirebaseAuthError() {
    if (_error != '') {
      return Container(
        padding: EdgeInsets.only(top: 10),
        child: Text(
          _error,
          style: TextStyle(color: Colors.red, fontSize: 14.0),
          textDirection: TextDirection.ltr,
        ),
      );
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
        width: 375,
        height: 812,
        padding: EdgeInsets.all(20),
        decoration: harmonyDecoration,
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            new Container(
              width: 164,
              height: 164,
              child: SvgPicture.asset("assets/logoHarmony.svg", color: themeColors['white'])
            ),
            new Container(
              width: 375,
              height: 100,
              margin: EdgeInsets.only(bottom: 100),
              child: new Center(
                child: new Text(
                  "HARMONY",
                  style: Theme.of(context).textTheme.display3,
                  textDirection: TextDirection.ltr,
                ),
              ),
            ),
            new GestureDetector(
              onTap: () => Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SignUp()),
              ),
              child: new Container(
                width: 375,
                height: 56,
                margin: EdgeInsets.only(bottom: 15, top: 20),
                decoration: new BoxDecoration(
                  color: themeColors['grayLight'],
                  borderRadius: BorderRadius.circular(4),
                  boxShadow: [BoxShadow(
                      color: themeColors['darkBoxShadow'],
                      offset: Offset(0,2),
                      blurRadius: 4,
                      spreadRadius: 0
                  )],
                ),
                child: new Center(
                    child: new Text("SIGN UP",
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        color: themeColors['gray'],
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                        decoration: TextDecoration.none,
                      ),
                      textDirection: TextDirection.ltr,
                    ),
                ),
              ),
            ),
            new GestureDetector(
              onTap: () => Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => UserPassLogin()),
              ),
              child: new Container(
                width: 375,
                height: 56,
                margin: EdgeInsets.only(bottom: 20),
                decoration: new BoxDecoration(
                  color: themeColors['grayDark'],
                  borderRadius: BorderRadius.circular(4),
                  boxShadow: [BoxShadow(
                      color: themeColors['darkBoxShadow'],
                      offset: Offset(0,2),
                      blurRadius: 4,
                      spreadRadius: 0
                  )],
                ),
                child: new Center(
                  child: new Text("LOG IN",
                        style: TextStyle(
                        fontFamily: 'Quicksand',
                        color: themeColors['white'],
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                        decoration: TextDecoration.none,
                      ),
                      textDirection: TextDirection.ltr,
                    ),
                ),
              ),
            ),
            _displayIfFirebaseAuthError(),
            new Center(
              child: GestureDetector(
                onTap: () async {
                  try {
                    await _auth.signInAnon();
                  } catch(e) {
                    setState(() => _error = e.message);
                  }
                },
                child: new Text(
                  "Skip, use our app without an account.",
                  style: Theme.of(context).textTheme.subhead,
                  textDirection: TextDirection.ltr,
                ),
              ),
            ),
          ],
        )
    );
  }
}
