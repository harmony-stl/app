import 'package:flutter/material.dart';
import 'package:mobile/services/auth.dart';
import 'package:mobile/services/theme.dart';
import 'package:mobile/services/util.dart';

class PasswordReset extends StatefulWidget {
  @override
  _PasswordResetState createState() => _PasswordResetState();
}

class _PasswordResetState extends State<PasswordReset> {
  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();

  String _email = '';
  String _error = '';

  Widget _displayIfFirebaseAuthError() {
    if (_error != '') {
      return Container(
        padding: EdgeInsets.only(top: 10),
        child: Text(
          _error,
          style: TextStyle(color: Colors.red, fontSize: 14.0),
        ),
      );
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: Icon(Icons.chevron_left),
            onPressed: () => Navigator.pop(context, false),
          )),
      body: new Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        decoration: harmonyDecoration,
        child: Form(
          key: _formKey,
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                height: 45,
                margin: EdgeInsets.only(bottom: 8),
                child: new Center(
                  child: new Text(
                    "Forgot Password?",
                    style: Theme.of(context).textTheme.display1,
                  ),
                ),
              ),
              new Container(
                height: 20,
                margin: EdgeInsets.only(bottom: 45),
                child: new Center(
                  child: new Text(
                    "Enter your email and we'll help",
                    style: Theme.of(context).textTheme.subhead,
                  ),
                ),
              ),
              TextFormField(
                style: TextStyle(
                  color: themeColors['gray'],
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                ),
                decoration:
                    InputDecoration(hintText: 'Email address').applyDefaults(
                  Theme.of(context).inputDecorationTheme,
                ),
                onChanged: (val) => setState(() => _email = val),
                validator: (val) => validateEmail(val),
              ),
              _displayIfFirebaseAuthError(),
              SizedBox(height: 25),
              new GestureDetector(
                onTap: () async {
                  if (_formKey.currentState.validate()) {
                    try {
                      await _auth.resetPassword(_email);
                      Navigator.pushReplacementNamed(context, "/resetConfirm");
                    } catch (e) {
                      setState(() {
                        _error = e.message;
                      });
                    }
                  }
                },
                child: new Container(
                  width: 375,
                  height: 56,
                  decoration: new BoxDecoration(
                    color: Color(0xffb0aeaf),
                    borderRadius: BorderRadius.circular(4),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0x7f000000),
                          offset: Offset(0, 2),
                          blurRadius: 4,
                          spreadRadius: 0)
                    ],
                  ),
                  child: new Center(
                    child: new Text(
                      "SEND",
                      style: Theme.of(context).textTheme.subhead.copyWith(
                            fontWeight: FontWeight.w700,
                            fontSize: 18,
                          ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
