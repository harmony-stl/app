import 'package:flutter/material.dart';
import 'package:mobile/layout/navbar.dart';

class Teaching extends StatefulWidget {
  @override
  _TeachingState createState() => _TeachingState();
}

class _TeachingState extends State<Teaching> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Navbar(),
    );
  }
}
