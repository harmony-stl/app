String validateEmail(val) {
  final RegExp _emailRegex = new RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
  return _emailRegex.hasMatch(val) ? null : 'Enter a valid email address.';
}

String validateComplexPassword(val) {
  dynamic hasUpperCase = new RegExp(r"^(?=.*[A-Z])").hasMatch(val);
  dynamic hasLowerCase = new RegExp(r"^(?=.*[a-z])").hasMatch(val);
  dynamic hasNumbers = new RegExp(r"^(?=.*\d)").hasMatch(val);
  dynamic hasNonalphas = new RegExp(r"^(?=.*\W)").hasMatch(val);

  print(hasNumbers);
  if (hasUpperCase && hasLowerCase && hasNumbers && hasNonalphas && val.length > 3 && !val.isEmpty) {
    return null;
  }

  return 'Password must be at least 4 characters and contain an uppercase letter, lowercase letter, number, and special character.';
}