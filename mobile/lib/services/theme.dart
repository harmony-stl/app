import 'package:flutter/material.dart';

final ThemeData harmonyTheme = _buildDefaultTheme();
final BoxDecoration harmonyDecoration = _buildDefaultDecoration();
final TextStyle harmonyNavItemStyle = _buildNavItemStyle(harmonyTheme);

final themeColors = {
  'white': Color(0xffffffff),
  'black': Color(0xff000000),
  'gray': Color(0xff222b45),
  'grayDark': Color(0xffb0aeaf),
  'grayLight': Color(0xffedf1f7),
  'grayPale': Color(0xffe4e9f2),
  'darkBoxShadow': Color(0x7f000000),
  'primaryColor': Color(0xff7E5773),
  'primaryColorLight': Color(0xffB0AEAF),
  'primaryColorDark': Color(0xff54214F),
};

ThemeData _buildDefaultTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    primaryColor: themeColors['primaryColor'],
    primaryColorLight: themeColors['primaryColorLight'],
    primaryColorDark: themeColors['primaryColorDark'],
    textTheme: _buildTextTheme(base),
    inputDecorationTheme: _buildTextInputDecoration(base),
  );
}

TextTheme _buildTextTheme(ThemeData base) {
  return TextTheme(
    display4: TextStyle(
      color: themeColors['black'],
      fontFamily: 'Quicksand',
      fontSize: 112,
    ),
    display3: TextStyle(
      color: themeColors['white'],
      fontFamily: 'Quicksand',
      fontSize: 56,
    ),
    display2: TextStyle(
      color: themeColors['white'],
      fontFamily: 'Quicksand',
      fontSize: 45,
    ),
    display1: TextStyle(
      color: themeColors['white'],
      fontFamily: 'Quicksand',
      fontSize: 34,
    ),
    subhead: TextStyle(
      color: themeColors['white'],
      fontFamily: 'Quicksand',
      fontSize: 16,
    ),
    body1: TextStyle(
      color: themeColors['black'],
      fontFamily: 'Quicksand',
      fontSize: 14,
    ),
  );
}

TextStyle _buildNavItemStyle(ThemeData harmonyTheme) {
  return TextStyle(
    fontFamily: 'AveriaSansLibre',
  );
}

InputDecorationTheme _buildTextInputDecoration(ThemeData base) {
  return InputDecorationTheme(
    fillColor: themeColors['white'],
    filled: true,
    contentPadding: EdgeInsets.symmetric(
      horizontal: 20,
      vertical: 10,
    ),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(4),
      borderSide: BorderSide(color: themeColors['grayPale'], width: 1.0),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(4),
      borderSide: BorderSide(color: themeColors['grayPale'], width: 1.0),
    ),
  );
}

BoxDecoration _buildDefaultDecoration() {
  return BoxDecoration(
    gradient: RadialGradient(
      center: const Alignment(-2, -1.7),
      radius: 2,
      colors: [
        themeColors['primaryColorLight'],
        themeColors['primaryColor'],
        themeColors['primaryColorDark'],
      ],
      stops: [0, 0.4301851530784485, 1],
    ),
  );
}
