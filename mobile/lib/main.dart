import 'package:flutter/material.dart';
import 'package:mobile/screens/authenticate/password_reset.dart';
import 'package:mobile/screens/authenticate/reset_confirm.dart';
import 'package:mobile/services/theme.dart';
import 'package:mobile/layout/wrapper.dart';
import 'package:mobile/services/auth.dart';
import 'package:provider/provider.dart';
import 'models/user.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: MaterialApp(
        initialRoute: "/",
        routes: {
          "/": (BuildContext context) => Wrapper(),
          "/resetPassword": (BuildContext context) => PasswordReset(),
          "/resetConfirm": (BuildContext context) => ResetConfirm(),
        },
        theme: harmonyTheme,
      ),
    );
  }
}
