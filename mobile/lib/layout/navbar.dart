import 'package:flutter/material.dart';
import 'package:mobile/services/auth.dart';
import 'package:mobile/services/theme.dart';

class Navbar extends StatefulWidget {
  @override
  _NavbarState createState() => _NavbarState();
}

class _NavbarState extends State<Navbar> {
  int _selectedIndex = 0;
  static final AuthService _auth = AuthService();
  
  void _onItemTapped(int index) async {
    setState(() {
      _selectedIndex = index;
    });
    if (_selectedIndex == 4) {
      await _auth.signOut();
    }
  }

  @override
  BottomNavigationBar build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: _selectedIndex,
      type: BottomNavigationBarType.fixed,
      selectedItemColor: themeColors['primaryColorDark'],
      unselectedItemColor: themeColors['primaryColorLight'],
      selectedLabelStyle: harmonyNavItemStyle,
      unselectedLabelStyle: harmonyNavItemStyle,
      showUnselectedLabels: true,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.import_contacts),
          title: Text('Teaching'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.near_me),
          title: Text('Serving'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.supervised_user_circle),
          title: Text('Community'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.music_note),
          title: Text('Worship'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.menu),
          title: Text('More'),
        ),
      ],
      onTap: _onItemTapped,
    );
  }
}
