// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:mobile/main.dart';
import 'package:mobile/screens/authenticate/authenticate.dart';

void main() {
  testWidgets('Skip login text is displayed on the sign in screen', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(Authenticate());

    // Verify that our skip text is on the screen
    expect(find.text('Skip, use our app without an account.'), findsOneWidget);
  });
}
