import 'package:flutter_test/flutter_test.dart';
import 'package:mobile/services/util.dart';

void main() {
  test('email should be validated', () { 
    expect(null, validateEmail("test@example.com"));
    expect('Enter a valid email address.', validateEmail("asdfasdf"));
    expect('Enter a valid email address.', validateEmail("test@"));
    expect('Enter a valid email address.', validateEmail("test@thing"));
    expect('Enter a valid email address.', validateEmail("@thing.com"));
    expect('Enter a valid email address.', validateEmail("noDomainSample@.com"));
  });

  test('password should be complex', () {
    final String failMessage = 'Password must be at least 4 characters and contain an uppercase letter, lowercase letter, number, and special character.';

    expect(null, validateComplexPassword("Pass123!"));
    expect(failMessage, validateComplexPassword("needupperalpha123!"));
    expect(failMessage, validateComplexPassword("NEEDLOWERAPLPHA123!"));
    expect(failMessage, validateComplexPassword("Neednumber!"));
    expect(failMessage, validateComplexPassword("Needspecial123"));
    expect(failMessage, validateComplexPassword(""));
  });
}
