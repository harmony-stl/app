# Mobile App
A new Flutter project.

# Run the mobile app
After ensuring that you have set up your machine for Flutter development using `flutter doctor` and seeing that everything is good, you can start the app by navigating into the `/mobile` folder (if not there already) and running `flutter run`. This will start the app running on the Simulator that you have open (should have had to have it open for `flutter doctor` to show you all was set).
> I tested running this on a mac with newest Xcode (Version 11.3.1 (11C504)) installed and running an iOS iPhone 11 emulator via mac's Simulator. I also tested on Android Studio (3.6.1) on the same mac with a Pixel 2 device from the AVD manager.

## Flutter Docs
This project is a starting point for a Flutter application.
A few resources to get you started if this is your first Flutter project:
- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)
For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
